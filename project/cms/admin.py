from django.contrib import admin
from .models import Contenidos, Comentario

# Register your models here.

admin.site.register(Contenidos)
admin.site.register(Comentario)